-- AstroCommunity: import any community modules here
-- We import this file in `lazy_setup.lua` before the `plugins/` folder.
-- This guarantees that the specs are processed before any user plugins.

---@type LazySpec
return {
	"AstroNvim/astrocommunity",
	{ import = "astrocommunity.code-runner.sniprun",           enabled = false },
	{ import = "astrocommunity.colorscheme.tokyonight-nvim" },
	{ import = "astrocommunity.recipes.heirline-vscode-winbar" },

	{ import = "astrocommunity.completion.copilot-lua",        enabled = true },
	{ import = "astrocommunity.completion.copilot-lua-cmp",    enabled = true },
	{
		"AstroNvim/astrocore",
		opts = { options = { g = { copilot_proxy = "127.0.0.1:2080" } } },
	},

	{ import = "astrocommunity.debugging.nvim-dap-virtual-text" },

	{ import = "astrocommunity.editing-support.chatgpt-nvim" },
	{ import = "astrocommunity.editing-support.cutlass-nvim" },
	{ "gbprod/cutlass.nvim",                                            opts = { cut_key = "c" } },
	{ import = "astrocommunity.editing-support.multicursors-nvim" },
	{ import = "astrocommunity.editing-support.refactoring-nvim" },
	{ import = "astrocommunity.editing-support.suda-vim" },
	{ import = "astrocommunity.editing-support.todo-comments-nvim" },
	{ import = "astrocommunity.editing-support.vim-move" },

	{ import = "astrocommunity.git.neogit" },
	{ import = "astrocommunity.media.presence-nvim" },

	{ import = "astrocommunity.pack.cpp" },
	{ import = "astrocommunity.pack.go" },
	{ import = "astrocommunity.pack.godot" },
	{ import = "astrocommunity.pack.java" },
	{ import = "astrocommunity.pack.just" },
	{ import = "astrocommunity.pack.lua" },
	{ import = "astrocommunity.pack.nix" },
	{ import = "astrocommunity.pack.rust" },
	{ import = "astrocommunity.pack.typescript-all-in-one" },

	{ import = "astrocommunity.editing-support.rainbow-delimiters-nvim" },
	{
		"lukas-reineke/indent-blankline.nvim",

		config = function(_, opts)
			local highlight = {
				"RainbowRed",
				"RainbowYellow",
				"RainbowBlue",
				"RainbowOrange",
				"RainbowGreen",
				"RainbowViolet",
				"RainbowCyan",
			}
			local hooks = require("ibl.hooks")
			-- create the highlight groups in the highlight setup hook, so they are reset
			-- every time the colorscheme changes
			hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
				vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
				vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
				vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
				vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
				vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
				vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
				vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })
			end)

			vim.g.rainbow_delimiters = { highlight = highlight }

			opts.scope.highlight = highlight

			require("ibl").setup(opts)

			hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)
		end,
	},

	{ import = "astrocommunity.git.git-blame-nvim" },
	{
		"f-person/git-blame.nvim",
		opts = function(_, opts)
			local cursorline_bg = vim.fn.synIDattr(vim.fn.hlID("CursorLine"), "bg#", "gui")
			local comment_fg = vim.fn.synIDattr(vim.fn.hlID("Comment"), "fg#", "gui")
			vim.api.nvim_set_hl(0, "_GitBlamePlugin", {
				bg = cursorline_bg,
				fg = comment_fg,
				italic = true,
			})

			opts.message_template = "<author> • <date> • <summary> "
			-- opts.date_format = "%c | %r"

			opts.message_when_not_committed = "Not commited"
			opts.display_virtual_text = 0
			-- opts.delay = 200
			opts.highlight_group = "_GitBlamePlugin"
		end,
	},
}
