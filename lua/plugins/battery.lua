return {
  'justinhj/battery.nvim',
  dependencies = {
    {'nvim-tree/nvim-web-devicons'}, {'nvim-lua/plenary.nvim'}
  },
  opts = {
    update_rate_seconds = 60,
    show_status_when_no_battery = false,
    show_plugged_icon = false,
    show_unplugged_icon = false
  }
}
