local GitBlameComponent = {
  -- The condition to check if blame info is available before displaying it
  condition = function()
    return require("gitblame").is_blame_text_available()
  end,
  -- The provider function to get the current blame text
  provider = function()
    return require("gitblame").get_current_blame_text()
  end,
  -- Highlight settings for the component
  hl = {
    fg = require("heirline.utils").get_highlight("Comment").fg,
    bg = require("heirline.utils").get_highlight("StatusLine").bg,
  },
}

local BatteryComponent = {
  -- the function that provides the text to be displayed
  provider = function()
    return require("battery").get_status_line()
  end,

  hl = function()
    return {
      fg = require("heirline.utils").get_highlight("Type").fg,
      bg = require("heirline.utils").get_highlight("StatusLine").bg,
    }
  end,
}

return {
  "rebelot/heirline.nvim",
  opts = function(_, opts)
    local status = require("astroui.status")

    opts.statusline = {                                                          -- statusline
      hl = { fg = "fg", bg = "bg" },
      status.component.mode({ mode_text = { padding = { left = 1, right = 1 } } }), -- add the mode text
      status.component.git_branch(),
      status.component.file_info({ filetype = {}, filename = false, file_modified = false }),
      status.component.diagnostics(),
      status.component.git_diff(),
      GitBlameComponent,
      status.component.fill(),
      status.component.cmd_info(),
      status.component.fill(),
      BatteryComponent,
      status.component.lsp(),
      status.component.treesitter(),
      status.component.nav(),
      -- remove the 2nd mode indicator on the right
    }

    return opts
  end,
}
