-- Customize Mason plugins

---@type LazySpec
return {
	{
		"williamboman/mason.nvim",
		opts = {
			PATH = "append",
			-- log_level = vim.log.levels.DEBUG,
		},
	},
}
