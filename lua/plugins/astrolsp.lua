-- AstroLSP allows you to customize the features in AstroNvim's LSP configuration engine
-- Configuration documentation can be found with `:h astrolsp`
-- NOTE: We highly recommend setting up the Lua Language Server (`:LspInstall lua_ls`)
--       as this provides autocomplete and documentation while editing

---@type LazySpec
return {
  "AstroNvim/astrolsp",
  opts = function(_, opts)
    opts.servers = require("astrocore").list_insert_unique(opts.servers, {
      -- "clangd",
      -- "gopls",
      -- "jdtls",
      -- "kotlin_language_server",
      -- "lemminx",
      "lua_ls",
      -- "rust_analyzer",
      -- "ltex"
    })
  end
}
